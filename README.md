You can access the api via below endpoint
    https://rdk-springboot-hello-world.herokuapp.com/hello

1. Create the Spring Boot Project in Spring Initializer
    https://start.spring.io/

2. Import the Maven Project in Eclipse
3. Add the below Dependency in pom.xml
    <dependencies>
        <dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter</artifactId>
		</dependency>
		
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-web</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
        </dependency>
    </dependencies>
4. Create the Hello World Controller
5. Right Click the POM file Choose Run As --> Maven Install
6. Right Click the POM file Choose Run As --> Maven Build(In Run Configuration put clean install)
7. Set the project target folder in cmd and run the program 
    java -jar springboot-hello-world-0.0.1-SNAPSHOT.jar
8. hit the below url and validate the service in postman
    http://localhost:8080/hello
9. Below Steps to Push the code to Gitlab
    git init
    git remote add origin https://gitlab.com/rdkinfoleaf/springboot-hello-world.git
    git add .
    git commit -m "Initial commit"
    git push -u origin master
10. Below steps to Deploy the service in heroku 
    $ heroku login
    $ cd my-project/
    $ git init
    $ heroku git:remote -a rdk-springboot-hello-world
    $ git add .
    $ git commit -am "make it better"
    $ git push heroku master
